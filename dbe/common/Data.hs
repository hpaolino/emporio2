{-# LANGUAGE ExistentialQuantification  #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
module Common.Data where

import           Data.Ratio
import           Data.Text
import           Data.Time.Calendar
import           Database.Persist.TH
share

    [ mkPersist sqlSettings { mpsGenerateLenses = True }
    , mkDeleteCascade sqlSettings
    , mkMigrate "migrateAll"
    ]

    [persistLowerCase|
Authentication json
    email Text
    cookie Text
    UniqueEmail email
    deriving Show
Store json
    name Text
    deriving Show Eq
Super
    email Text
    deriving Show
Admin json
    email Text
    store StoreId
    deriving Show
Operator json
    email Text
    nickname Text
    store StoreId
    state Text
    deriving Show
User json
    card Int
    email Text
    nickname Text
    points Rational
    store StoreId
    state Text
    deriving Show
Product json
    name Text
    points Rational
    image Text
    store StoreId
    state Text
    deriving Show
Shopping json
    product ProductId
    count Int
    value Rational
    user UserId
    date Day
    deriving Show
|]
