{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE ExistentialQuantification  #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}

module Authenticate where

import           Common.Communication
import           Control.Monad
import           Control.Monad.Logger          (LoggingT, runStdoutLoggingT)
import           Control.Monad.Trans
import           Common.Data
import           Data.Aeson                    hiding (json)
import           Data.HVect                    hiding (pack)
import           Data.Maybe
import           Data.Monoid                   ((<>))
import           Data.Set                      (Set)
import qualified Data.Set                      as Set
import           Data.Text                     (Text, pack, unpack)
import           Database.Persist              hiding (get)
import qualified Database.Persist              as P
import           Database.Persist.Sqlite       hiding (get)
import           Database.Persist.TH
import           Lib
import           Mailing
import           Network.Wai.Middleware.Static
import           System.Random
import           Web.Spock
import           Web.Spock.Config


registerA :: Text -> ApiAction ctx ()
registerA lg = do
    m <- runSQL $ getBy (UniqueEmail lg)
    case m of
        Nothing           -> jsonError 1 "mail not registered"
        Just (Entity k _) -> do
            c <- createCookie
            runSQL $ replace k (Authentication lg c)
            liftIO $ sendEmail lg c

upsertAuthentication :: Text -> ApiAction ctx ()
upsertAuthentication e = do
    exist <- runSQL $ getBy (UniqueEmail e)
    case exist of
        Nothing -> do
            runSQL $ insert (Authentication e "")
            registerA e
            return ()
        _ -> return ()

createCookie :: MonadIO m => m Text
createCookie = liftIO $ do
    c <- randomRIO (10 ^ 12, 10 ^ 13 :: Int)
    return $ pack . show $ c

maybeAuthentication
    :: (Either Text Authentication -> ApiAction ctx a) -> ApiAction ctx a
maybeAuthentication action = do
    sess <- readSession
    case sess of
        Nothing -> action $ Left "no session"
        Just s  -> do
            mAuthentication <- runSQL
                $ selectList [AuthenticationCookie ==. s] []
            action $ case mAuthentication of
                []           -> Left "not authenticated"
                [Entity _ x] -> Right x
                _            -> error "repeated cookie"

baseH :: ApiAction () (HVect '[])
baseH = return HNil

authH :: ApiAction (HVect xs) (HVect (Authentication : xs))
authH = maybeAuthentication $ \mAuthentication -> do
    oldCtx <- getContext
    case mAuthentication of
        Left  e    -> jsonError 1 e
        Right auth -> return (auth :&: oldCtx)



type Check xs a = ApiAction (HVect xs) a -> ApiAction (HVect xs) a

roleH
    :: ListContains n WhoAmI xs
    => [Role]
    -> Check xs a
roleH rs a = do
    WhoAmI _ _ s <- findFirst <$> getContext
    if any (`Set.member` s) rs then a
    else jsonError 1 "authorization issue"


superH
    :: ListContains n WhoAmI xs
    => Check xs a
superH = roleH [RSuper]

adminH
    :: ListContains n WhoAmI xs
    => Key Store
    -> Check xs a
adminH r = roleH [RAdmin r]

operatorH r = roleH [ROperator r]



