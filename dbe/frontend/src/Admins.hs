{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoMonomorphismRestriction  #-}
{-# LANGUAGE OverloadedLists            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE RecursiveDo                #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Admins where

import           Data.Aeson           hiding (json)
import           Data.Monoid          ((<>))
import           Data.Text            (Text, pack, unpack)
import qualified Data.Text            as T
import           Data.Text.Encoding

import           Common.Communication
import           Control.Lens         ((^.))
import           Control.Monad
import           Common.Data
import           Data.List
import           Data.Map             (Map)
import qualified Data.Map             as M
import           Data.Set             (Set)
import qualified Data.Set             as Set
import           Database.Persist     hiding (get)
import           Database.Persist
import           Database.Persist.Sql
import           Lib
import           Reflex
import           Reflex.Dom           hiding (Key, button, link, table)
import qualified Reflex.Dom
import           Roles
import           Row

updateRow :: ( MS m) => Maybe Admin -> m (ES FieldUpdate)
updateRow (Just (Admin n _)) = rowWTdp
        [ ( n , \x -> if "@" `T.isInfixOf` x
                    then Just $ FU "email" x
                    else Nothing
          )
        ]
updateRow Nothing = spinner >> return never

admins :: MS m => Entity Store -> Maybe [Entity Admin] -> m (ES ())
admins _              Nothing   = spinner >> return never
admins (Entity sid t) (Just us) = do
    divClass "subtitle" $ text $ "Anagrafe amministratori di " <> t ^. storeName
    table $ do
        s <- fmap leftmost $ forM us $ \y@(Entity k x) -> el "tr" $ do
            aE <- rowUpdateNet y "admin" updateRow
            d <- elClass "td" "delete" $ icon ["trash"]
            dd <- modal $ ("elimina amministratore " <> x ^. adminEmail <> " ?") <$ d
            dE <-fmap (fmap (\(_ :: Maybe ()) -> ()))
                $  getAndDecode
                $  ("admins/delete/" <> renderKey k)
                <$ dd
            return $ leftmost [() <$ aE, dE]
        a <- el "tr" $ do
            a <- newRowTdp
                $ NewRow
                      [ (  "indirizzo mail"
                        , \x -> "@" `T.isInfixOf` x
                        )
                      ]
                    (\[t] -> Just $ Admin t sid)
                    (el "td" $ icon ["ban"] >> return ())
                $ el "td" $ icon ["check"]
            rowNewNet "admin" a
        return $ leftmost [() <$ a, s]

adminsWidget :: MS m => Entity Store -> m (ES ())
adminsWidget z@(Entity k s) = do
    let ep = "admins/" <> renderKey k
    rec
        ms <- getPostBuild
            >>= \pb -> getAndDecode (ep <$ leftmost [pb, r])
        ps :: DS (Maybe [Entity Admin]) <- holdDyn Nothing $ ms
        r                               <- domMorph (admins z) ps
    return r


