{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoMonomorphismRestriction  #-}
{-# LANGUAGE OverloadedLists            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE RecursiveDo                #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Operators where

import           Data.Aeson           hiding (json)
import           Data.Monoid          ((<>))
import           Data.Text            (Text, pack, unpack)
import qualified Data.Text            as T
import           Data.Text.Encoding

import           Common.Communication
import           Control.Lens         ((^.))
import           Control.Monad
import           Common.Data
import           Data.List
import           Data.Map             (Map)
import qualified Data.Map             as M
import           Data.Maybe
import           Data.Set             (Set)
import qualified Data.Set             as Set
import           Database.Persist     hiding (get)
import           Database.Persist
import           Database.Persist.Sql
import           Lib
import           Reflex
import           Reflex.Dom           hiding (Key, button, link, table)
import qualified Reflex.Dom
import           Roles
import           Row


-- updateRow :: MS m => Maybe Product -> m (ES )
updateRow (Just pr@(Operator e n _ s)) = rowWTdp
        [ (e, \x -> if "@" `T.isInfixOf` x
                    then Just $ FU "email" x
                    else Nothing
          )
        , (n, \x -> Just (FU "nickname" x))
        , (s, \case
                    "A" -> Just (FU "state" ("A" :: Text))
                    "P" ->  Just (FU "state" ("P" :: Text))
                    _ -> Nothing
          )
        ]
updateRow Nothing = spinner >> return never

makeRow :: MS m => Key Store -> m (ES Operator)
makeRow k
    = newRowTdp
    $ NewRow
        [ ( "email cassiere", \x -> "@" `T.isInfixOf` x)
        , ( "nick name", const True)
        , ( "stato", (`T.isInfixOf` "AP"))
        ]
        (\[e,n,s] -> Operator <$> pure e <*> pure n <*> pure k <*> pure s)
        (el "td" $ icon ["ban"] >> return ())
        $ el "td" $ icon ["check"]

operators :: MS m => Key Store -> Maybe [Entity Operator] -> m (ES ())
operators _ Nothing   = spinnerE
operators k (Just us) = do
    divClass "subtitle" $ do
        text "Anagrafe Cassieri"
    r <- divClass "operators" $ table $ do
        el "thead" $ el "tr" $ do
            mapM_ (el "th" . text)
                (["email","nome","stato","-"] :: [Text])
        d <- fmap leftmost $ forM us $ \y@(Entity rk x) ->
            el "tr" $ do
                rowUpdateNet y ("operator/" <> renderKey k) updateRow
                d <- elClass "td" "delete" $ icon ["trash"]
                dd <- modal $ ("elimina cassiere " <> x ^. operatorNickname <> " ?") <$ d
                ddd <- modal $ ("elimina veramente cassiere " <> x ^. operatorNickname <> " ?") <$ dd
                fmap (fmap (\(_ :: Maybe ()) -> ()))
                    $  getAndDecode
                    $  ("operators/" <> renderKey k <> "/delete/" <> renderKey rk)
                    <$ ddd
        a <- fmap (() <$) $ el "tr" $
            makeRow k >>= rowNewNet ("operator/" <> renderKey k)
        return $ leftmost [a,d]
    return r

operatorsWidget :: MS m => Key Store -> m (ES ())
operatorsWidget k = do
    let ep = "operators/" <> renderKey k
    pb <-  getPostBuild
    rec
        ms <- getAndDecode $ ep <$ leftmost [pb, s]
        ps :: DS (Maybe [Entity Operator]) <- holdDyn Nothing $ ms
        s <- domMorph (operators k) ps
    return s

