{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE IncoherentInstances        #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoMonomorphismRestriction  #-}
{-# LANGUAGE OverloadedLists            #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE Rank2Types                 #-}
{-# LANGUAGE RecursiveDo                #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances       #-}

module Cashing where

import           Common.Communication
import           Control.Lens         ((^.))
import           Control.Lens         hiding (Context)
import           Control.Monad
import           Control.Monad.Trans
import           Data.Aeson           hiding (json)
import           Data.List
import           Data.Map             (Map)
import qualified Data.Map             as M
import           Data.Maybe
import           Data.Monoid          ((<>))
import           Data.Set             (Set)
import qualified Data.Set             as Set
import           Data.Text            (Text, pack, strip, unpack)
import qualified Data.Text            as T
import           Data.Text.Encoding
import           Database.Persist     hiding (get)
import           Database.Persist
import           Database.Persist.Sql
import           Reflex
import           Reflex.Dom           hiding (Key, button, link, table)
import qualified Reflex.Dom

import           Common.Data
import           Lib
import           Common.Rational
import           Roles
import           Row

-- | button
button0 :: MS m => Text -> m a -> m (ES ())
button0 c content = do
    (e, _) <- elClass' "button" c content
    return $ domEvent Click e

instance ToBackendKey SqlBackend a => Ord (Key a) where
    k `compare` k' = fromSqlKey k `compare` fromSqlKey k'
instance ToBackendKey SqlBackend a => Eq (Key a) where
    k == k' = fromSqlKey k == fromSqlKey k'

type MM a b = M.Map (Key a) b
type MMK a = MM a a

fromEntities :: ToBackendKey SqlBackend a => [Entity a] -> MMK a
fromEntities es = M.fromList [(k,v) | Entity k v <- es]

type Cart = MM Product Int
data UserContext = UserContext
    { _userCart  :: Cart
    , _userDebit :: Rational
    }

makeLenses ''UserContext

type Carts = MM User UserContext

data State = State {
    _productRow   :: Int,
    _selectedUser :: Maybe (Key User),
    _carts        :: Carts
    }

makeLenses ''State

zeroState = State 4 Nothing mempty

data Context = Context {
    store    :: Key Store,
    users    :: MMK User,
    products :: MMK Product
    }



type Card = Int

type Points = Rational
data Bill = Bill {
    shopping    :: [Shopping]
    , leftOvers :: Rational
    }

showBill :: MS m => Key User -> Context -> State -> m ()
showBill u c@(Context _ us ps) s = do
        divClass "subtitle" $ text "Scontrino"
        let bs = shopping $ bill u c s
        divClass "bill" $ table $ do
            el "thead" $ el "tr" $ do
                el "th" $ text "articolo"
                el "th" $ text "confezioni"
                el "th" $ text "punti"
            forM_ (sortOn ((\pk -> ps ^?! ix pk . productName)
                    . view shoppingProduct) bs)
                $ \(Shopping pk t v _ u) ->
                    el "tr" $ do
                        elClass "td" "product" $ text $ ps ^?! ix pk . productName
                        elClass "td" "quantity" $ text $ pshow $ t
                        elClass "td" "value" $ text $ r1show $ v
            el "tfoot" $ el "tr" $ do
                el "td" $ text "totale"
                el "td" $ text $ pshow (sum . map (view shoppingCount) $ bs)
                el "td" $ text $ r1show (sum . map (view shoppingValue) $ bs)

bill :: Key User -> Context -> State -> Bill
bill u ctx@(Context _ us ps) s@(State _ _ cs) =
    let f (pk, t)
            | t > 0 = [Shopping pk t (fromIntegral t * value) u (toEnum 0)]
            | otherwise = []
            where Product name value _ _ _ = ps ^?! ix pk
        bs = concatMap f $ M.assocs $ cs ^?! ix u . userCart
        w = leftOver u ctx s bs
        in Bill bs w

leftOver :: Key User -> Context -> State -> [Shopping] -> Rational
leftOver ku ctx (State _ _ cs) xs
    = users ctx ^?! ix ku . userPoints - cs ^?! ix ku . userDebit - (sum . map (view shoppingValue)) xs

iAttr i x =
    [ ("src"   ,i)
    , ("height", pshow x)
    , ("width" , pshow x)
    ]
-- | product key
prodotto
    :: MS m
    => Context
    -> DS Bill
    -> DS Int -- delta (pos neg)
    -> DS Int -- size
    -> Key Product
    -> DS Int  -- count
    -> m (ES Int) -- delta event
prodotto (Context _ us ps) billD opbD szD kp nD  = el "li" $ do
    -- getPostBuild >>= \pb -> performEvent_ (liftIO (putStrLn "bbb") <$ pb)
    let Product t n i _ _ =  ps M.! kp
        content = do
            elDynAttr "img" (iAttr i <$> szD) $ return ()
            divClass "testuale" $ text (t <> ", " <> r1show n)
            elClass "div" "count" $ dynText $  pshow <$> nD
    e <- button0 "btn btn-primary" $ content
    let check d (Bill _ w) = w - fromIntegral d * n >= 0
    return $ current opbD <@ gate (check <$> current opbD  <*> current billD) e
    -- return $ current opbD <@ e


control
    :: MS m
    => Key User
    -> Context
    -> DS State
    -> Int -- actual number of keys per row
    -> m (DS Int, ES (), DS Int, DS Int, ES ())
control u ctx state productRow
    = elClass "ul" "control" $ do
        larger <- fmap ((+1) <$)
                . el "li"
                $ icon ["search-minus"] -- (text "<->")
        smaller <- fmap (subtract 1 <$)
                . el "li"
                $ icon ["search-plus"] -- (text ">-<")
        let chsz = leftmost [larger, smaller]
        sz <- foldDyn (\f x -> max 1 (f x)) productRow $ chsz
        r <- getPostBuild
        w <- iw $ leftmost [() <$ chsz, r]
        let f n w = (w - 47 - (n + 1) * 3) `div` n
        sz1 <- holdDyn 80 $ attachWith f (current sz) w
        reset <- el "li" $ abort
        user <- el "li" $ icon ["user"] -- text "UTENTE"
        let sw False = icon ["plus"]
            sw True  = icon ["minus"]
        rec e  <- el "li" $ domMorph sw opb
            opb <- foldDyn ($) True $
                leftmost    [   const not <$> e
                            ,   const True <$ reset
                            ]
        commit <- el "li" $ domMorph (close u ctx) state
        return (ite 1 (-1) <$> opb, leftmost [reset, commit], sz1,sz, user)

findCard :: Card -> Context -> Maybe (Key User, User)
findCard k (Context _ us _)
    = find ((==) k . view userCard . snd)
    . M.assocs
    $ us

getNewUser :: MS m => Context -> m (ES (Key User))
getNewUser ctx@(Context _ us _)  = divClass "new_user" $ do
    let tryRead s = case reads . unpack . strip $ s of
                     [(u,"")] -> Just u
                     _        -> Nothing
    rec     t <- elClass "span" "enter_card"
                $ textInput $
                    def
                    & textInputConfig_setValue .~ e
                    & textInputConfig_attributes
                                .~ constDyn [("placeholder", "ingresso tessera")]

            let e = "" <$ keypress Enter t
                g :: DS Bool
                g = (/= "") <$> t ^. textInput_value
            elClass "span" "report_card" $ dyn $ flip fmap ((,,) <$> r <*> s <*> g)
                $ \(r,s,g) ->
                case g of
                    False -> return ()
                    True -> case r of
                        Nothing -> elClass "span" "noparse"
                            $ text "non è un numero"
                        Just t -> case s of
                            Nothing -> elClass "span" "nocard"
                                $ text $ "tessera inesistente"
                            Just u -> elClass "span" "user"
                                $ text (view userNickname . snd $ u)
            r <- holdDyn Nothing $ tryRead <$> _textInput_input t
            s <- holdDyn Nothing $ fmap (\r -> r >>= \k -> findCard k ctx)
                    (updated r)

    return $ fmapMaybe id $ tag (fmap fst <$> current s) e

getUserDebit :: MS m => Context -> ES (Key User) -> m (ES Rational)
getUserDebit ctx kuE = do
    sps <- getAndDecode $ (\ku ->
        "shopping/" <> renderKey (store ctx) <> "/" <> renderKey ku) <$> kuE
    let t ps = sum . map (view shoppingValue) $ ps
    return $ t <$> fmapMaybe id sps




insertUser :: (Key User, Rational) -> State -> State
insertUser (u, w)
    =   over carts (M.insert u (UserContext mempty w))

deleteUser :: Key User -> State -> State
deleteUser u
    =  over selectedUser (\case
                         Nothing -> Nothing
                         Just u' -> if u == u' then Nothing else Just u'
                         )
    .  over carts (M.delete u)



close :: MS m => Key User -> Context -> State -> m (ES ())
close u ctx@(Context ks us _) s = do
    c <- icon ["check"]
    let un = us ^?! ix u . userNickname
        uc = us ^?! ix u . userCard
    r <-
        modalM
        $  ("Concludere la spesa ?"
           , showBill u ctx s
           )
        <$ c
    z <-
        performRequestAsync
        $  (postJson ("shopping/" <> renderKey ks) $ shopping $ bill u ctx s)
        <$ r
    return $ () <$ z

abort = do
    d <- icon ["trash"]
    modal $ "Abbandona il carrello ?" <$ d

application :: MS m => Context -> State -> m (ES (Either State State))
application c@(Context _ us bs) s0@(State _ Nothing _) = do
    upd <- divClass "subtitle" $ do
        upd <- elClass "span" "refresh" $ icon ["refresh"]
        el "span" $ text "Nuovo Utente"
        return upd
    nuE <- getNewUser c
    ucE <- coupleWith (getUserDebit c) nuE
    rec
        let ana True = return never
            ana False = do
                divClass "subtitle" $ do
                    text "Utenti serviti"
                fmap (fmap (snd . head . M.assocs))
                        $ table $ do
                            el "thead" $ el "tr" $ do
                                mapM_ (el "th" . text)
                                    (["nome","tessera","residuo", "","",""] :: [Text])
                            listViewWithKey (view carts <$> s)
                                $ \u uctxD -> do
                                el "tr" $ do
                                        el "td" $ text (us ^?! ix u . userNickname)
                                        el "td" $ text
                                            $ pshow $ us ^?! ix u . userCard
                                        el "td" $ dynText $ r1show
                                            <$> flip subtract (users c ^?! ix u . userPoints)
                                            <$> (view userDebit <$> uctxD)
                                        d <- el "td" $  abort
                                        c <- el "td" $  domMorph (close u c) s
                                        e <- el "td" $ icon ["shopping-cart"]
                                        return $ leftmost [ Left u <$ leftmost [d,c]
                                                            , Right u <$ e
                                                            ]
        ope <- domMorph ana (null . view carts <$> s)
        s <- foldDyn ($) s0 $ leftmost
            [ insertUser <$> ucE
            , deleteUser <$> filterLeft ope
            ]
    return $ leftmost $
        [ fmap Right $ attachWith (\s u -> set selectedUser (Just u) s)  (current s) $ filterRight ope
        , Left <$> tag (current s) upd
        ]
application ctx@(Context _ us ps) s0@(State p0 (Just serving) ctxs)
    = do
        rec
            let billD = bill serving ctx <$> s
            upd <- divClass "subtitle" $ do
                upd <- elClass "span" "refresh" $ icon ["refresh"]
                elClass "span" "bill_nickname"
                    $ text $  (us ^?! ix serving . userNickname )
                elClass "span" "bill_card"
                    $ text $ "(" <> pshow (us ^?! ix serving . userCard) <> ")"
                elClass "span" "bill_leftOvers"
                    $ dynText $ r1show <$> leftOvers <$> billD
                return upd
            (opbD, delE, szD, prD, buE) <- control serving ctx s p0
            s  <- elClass "ul" "tasti" $ do
                rec     mc <- listViewWithKey
                            (sortS ps <$> (\s -> populateCart ctx $ s ^?! selC serving) <$> s)
                            (prodotto ctx billD opbD szD . snd)
                        s <- foldDyn ($) s0
                                $ (\n -> over (selC serving) $ M.unionWith minzero n)
                                <$> unsortS <$> mc
                return s
            let as = over (selC serving)  amendCart <$> (set productRow <$> prD <*> s)
        return $ leftmost $
            [ fmap Right $ leftmost
                [ attachWith (\s u -> deleteUser u s) (current as) $ serving <$ delE
                , tag (set selectedUser Nothing <$> current as) buE
                ]
            , Left <$> tag (current as) upd
            ]

sortS ps = M.mapKeys $ \k -> (ps ^?! ix k . productName, k)
unsortS = M.mapKeys snd

populateCart :: Context -> Cart -> Cart
populateCart (Context _ _ ps) m = foldr (\k -> M.insertWith max k 0) m (M.keys ps)

amendCart :: Cart -> Cart
amendCart  = M.filter (>0)

selC :: Applicative f => Key User -> LensLike' f State (MM Product Int)
selC i = carts . ix i . userCart

testApply :: (State -> Bool) -> (State -> State) -> State -> State
testApply t c s
    | t (c s) = c s
    | otherwise = s


cashingW :: MS m => Key Store -> m (ES ())
cashingW s = do
    pb <- getPostBuild
    rec ctxD <- getContext s $ leftmost [pb, () <$ filterLeft sE]
        sD <- holdDyn zeroState (either id id <$> sE)
        sE <- domMorph (uncurry application) $ (,) <$> ctxD <*> sD
    return never

getContext :: MS m => Key Store -> ES () -> m (DS Context)
getContext ks e = do
    let ksE = ks <$ e
    uE :: ES (Maybe [Entity User])
        <- getAndDecode $ (\ks -> "users/" <> renderKey ks) <$> ksE
    uD <- holdDyn [] $ fmapMaybe id uE
    pE :: ES (Maybe [Entity Product])
        <- getAndDecode $ (\ks -> "products/" <> renderKey ks <> "/selling") <$> ksE
    pD <- holdDyn [] $ fmapMaybe id pE
    return $ Context ks <$> (fromEntities <$> uD) <*> (fromEntities <$> pD)
{-
        let setCtx :: Functor f => f Ctx -> f CtxMap
            setCtx
                = fmap
                $ \d ->  M.insert servedCard d ctxs
            rmCtx :: CtxMap
            rmCtx = M.delete servedCard ctxs
            resetting
                = Modal (return ())
                        ("Azzerare la spesa di " <> showCard servedCard <> " ?")
                <$> attach
                    ( MakeCart
                        <$> ( State
                            <$> current prD
                            <*> pure (Just servedCard)
                            <*> setCtx (current d)
                            )
                    )
                    ( MakeCart
                        <$> attachWith
                            (\d e -> State d (Just servedCard) e)
                            (current prD)
                            (setCtx $ over cart (fmap (const 0)) ctx <$ reset)
                    )
            committing
                = attachWith
                    (\s1 s2  -> Modal (domMorph (\x -> showBill x >> return never) d >> return ())
                        ("Concludere la spesa di " <> showCard servedCard <> " ?")
                        (MakeCart s1, DefineUsers s2)
                    )
                    (State <$> current prD <*> pure (Just servedCard) <*> setCtx (current d))
                    $ attachWith
                        (\d e -> State d (Just servedCard) e)
                        (current prD)
                        (rmCtx <$ commit)
            userview
                = DefineUsers
                <$> tag
                    (State <$> current prD <*> pure Nothing <*> setCtx (current d))
                    bu
        return $ wire' Desking $ leftmost [resetting, userview, committing]



initialView :: View m State
initialView = DefineUsers initialState

initialState = State 3 Nothing mempty

-}

