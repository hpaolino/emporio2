{ mkDerivation, aeson, base, bifunctors, containers, csv
, data-default, dependent-map, dependent-sum
, dependent-sum-template, ghcjs-dom, jsaddle, jsaddle-dom, lens
, mtl, persistent, persistent-template, pwstore-purehaskell, random
, reflex, reflex-dom, stdenv, text, time, uri
}:
mkDerivation {
  pname = "frontend";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    aeson base bifunctors containers csv data-default dependent-map
    dependent-sum dependent-sum-template ghcjs-dom jsaddle jsaddle-dom
    lens mtl persistent persistent-template pwstore-purehaskell random
    reflex reflex-dom text time uri
  ];
  homepage = "https://github.com/githubuser/Spock-rest#readme";
  license = stdenv.lib.licenses.bsd3;
}
